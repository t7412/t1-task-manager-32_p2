package ru.t1.chubarov.tm.enpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.chubarov.tm.api.service.IDomainService;
import ru.t1.chubarov.tm.api.service.IServiceLocator;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;
import ru.t1.chubarov.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @Override
    @NotNull
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @Override
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
