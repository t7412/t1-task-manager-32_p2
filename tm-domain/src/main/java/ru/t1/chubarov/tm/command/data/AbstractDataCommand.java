package ru.t1.chubarov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.service.IDomainService;
import ru.t1.chubarov.tm.api.service.IServiceLocator;
import ru.t1.chubarov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainService getDomainService() {
        return domainService;
    }

//    @NotNull
//    public static final String FILE_BASE_64 = "./data.base64";
//    @NotNull
//    public static final String FILE_BINARY = "./data.bit";
//    @NotNull
//    public static final String FILE_XML = "./data.xml";
//    @NotNull
//    public static final String FILE_JSON = "./data.json";
//    @NotNull
//    public static final String FILE_YAML = "./data.yaml";
    @NotNull
    public static final String FILE_BACKUP = "./data_backup.base64";
//    @NotNull
//    public final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";
//    @NotNull
//    public final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";
//    @NotNull
//    public final String MEDIA_TYPE = "eclipcelink.media-type";
//    @NotNull
//    public final String APPLICATION_TYPE_JSON = "application/json";
//
//    public AbstractDataCommand() {
//    }
//
//    @NotNull
//    public Domain getDomain() {
//        @NotNull final Domain domain = new Domain();
//        domain.setProjects(serviceLocator.getProjectService().findAll());
//        domain.setTasks(serviceLocator.getTaskService().findAll());
//        domain.setUsers(serviceLocator.getUserService().findAll());
//        return domain;
//    }
//
//    public void setDomain(@Nullable final Domain domain) {
//        if (domain == null) return;
//        serviceLocator.getProjectService().set(domain.getProjects());
//        serviceLocator.getTaskService().set(domain.getTasks());
//        serviceLocator.getUserService().set(domain.getUsers());
//        serviceLocator.getAuthService().logout();
//    }

}
