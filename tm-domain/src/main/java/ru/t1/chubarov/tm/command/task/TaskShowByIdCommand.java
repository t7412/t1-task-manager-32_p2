package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = getTaskService().findOneById(id);
        @NotNull final String userId = getUserId();
        showTask(userId, task);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by id.";
    }

}
