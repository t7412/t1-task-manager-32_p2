package ru.t1.chubarov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

public interface ITaskEndpoint {

    @NotNull
    @SneakyThrows
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    @SneakyThrows
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    @SneakyThrows
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    @SneakyThrows
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    @SneakyThrows
    TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request);

    @NotNull
    @SneakyThrows
    TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request);

    @NotNull
    @SneakyThrows
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    @SneakyThrows
    TaskListByProjectIdResponse listTaskByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    @SneakyThrows
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    @SneakyThrows
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    @SneakyThrows
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    @SneakyThrows
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

}
