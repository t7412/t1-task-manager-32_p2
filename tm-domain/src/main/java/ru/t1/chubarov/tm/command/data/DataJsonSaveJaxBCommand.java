package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-json-jaxb";
    @NotNull
    private final String DESCRIPTION = "Save data in json file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getDomainService().saveDataJsonJaxB();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
