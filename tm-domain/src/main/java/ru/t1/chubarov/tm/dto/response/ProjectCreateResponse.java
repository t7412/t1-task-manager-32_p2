package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.chubarov.tm.model.Project;

@Getter
@Setter
public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(Project project) {
        super(project);
    }

}
