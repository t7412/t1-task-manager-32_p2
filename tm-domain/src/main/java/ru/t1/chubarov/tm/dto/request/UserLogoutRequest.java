package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class UserLogoutRequest extends AbstractUserRequest {
}
