package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private Integer index;

}
