package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractUserResponse {

    public UserLoginResponse(@Nullable Throwable throwable) {
        super(throwable);
    }

}
