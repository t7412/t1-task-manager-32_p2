package ru.t1.chubarov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull
    @SneakyThrows
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    @SneakyThrows
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    @SneakyThrows
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectShowResponse listProject(@NotNull ProjectShowRequest request);

    @NotNull
    @SneakyThrows
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectTaskBindToProjectResponse bindTaskToProject(@NotNull ProjectTaskBindToProjectRequest request);

    @NotNull
    @SneakyThrows
    ProjectTaskUnbindToProjectResponse unbindTaskToProject(@NotNull ProjectTaskUnbindToProjectRequest request);

    @NotNull
    @SneakyThrows
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
