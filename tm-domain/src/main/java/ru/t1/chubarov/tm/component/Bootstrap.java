package ru.t1.chubarov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.chubarov.tm.exception.system.CommandNotSupportedException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.CommandRepository;
import ru.t1.chubarov.tm.repository.ProjectRepository;
import ru.t1.chubarov.tm.repository.TaskRepository;
import ru.t1.chubarov.tm.repository.UserRepository;
import ru.t1.chubarov.tm.service.*;
import ru.t1.chubarov.tm.util.SystemUtil;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.chubarov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() throws AbstractException {

        @NotNull final User test = userService.create("test", "test", "test@emal.ru");
        @NotNull final User user = userService.create("user", "user", "user@emal.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("ALFA PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("BETA PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("DELTA PROJECT", Status.COMPLETED));
        taskService.create(test.getId(), "FIRST TASK", "FIRST DESCRIPTION");
        taskService.create(test.getId(), "SECOND TASK", "SECOND DESCRIPTION");
    }

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> claszz : classes) {
            try {
                regestry(claszz);
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
    }



    private void regestry(@NotNull final Class<? extends AbstractCommand> clazz) throws IllegalAccessException, InstantiationException {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        regestry(command);
    }

    private void regestry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) throws AbstractException {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() throws AbstractException {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        backup.start();
        fileScanner.start();
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                prepareShutdown();
            }
        });
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
        fileScanner.stop();
        server.stop();
    }

    private boolean processArguments(@Nullable final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(@Nullable final String arg) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) throws AbstractException {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
