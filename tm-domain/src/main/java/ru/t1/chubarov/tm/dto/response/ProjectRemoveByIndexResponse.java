package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.chubarov.tm.model.Project;

@Getter
@Setter
public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(Project project) {
        super(project);
    }

}
