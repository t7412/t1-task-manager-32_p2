package ru.t1.chubarov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.Operation;
import ru.t1.chubarov.tm.api.service.IAuthService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.IServiceLocator;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.dto.request.AbstractRequest;
import ru.t1.chubarov.tm.dto.response.AbstractResponse;
import ru.t1.chubarov.tm.task.AbstractServerTask;
import ru.t1.chubarov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @Getter
    @Nullable
    private ServerSocket socketServer;

    @Getter
    @NotNull
    private final Bootstrap bootstrap;

    @SneakyThrows
    public Server(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        socketServer = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ,RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (socketServer == null) return;
        executorService.shutdown();
        socketServer.close();
    }

}
