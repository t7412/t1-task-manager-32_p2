package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.chubarov.tm.model.Project;

@Getter
@Setter
public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(Project project) {
        super(project);
    }

}
