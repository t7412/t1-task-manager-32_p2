package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String discription;

//    public ProjectCreateRequest(@Nullable String name, @Nullable String discription) {
//        this.name = name;
//        this.discription = discription;
//    }

}
