package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

@Getter
@Setter
public final class TaskListByProjectIdResponse extends AbstractTaskResponse {

    @Nullable
    private List<Task> tasks;

    public TaskListByProjectIdResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

}
