package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Project;

import java.util.List;

@Getter
@Setter
public final class ProjectShowResponse extends AbstractProjectResponse {

    @Nullable
    private List<Project> projects;

    public ProjectShowResponse(@Nullable List<Project> projects) {
        this.projects = projects;
    }

}
