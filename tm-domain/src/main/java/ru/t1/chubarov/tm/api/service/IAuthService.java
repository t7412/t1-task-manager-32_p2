package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.user.AccessDeniedException;
import ru.t1.chubarov.tm.model.User;

public interface IAuthService {

    void login(@NotNull String login, @NotNull String password) throws AbstractException;

    User check(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId() throws AccessDeniedException;

    @NotNull
    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role[] roles) throws AbstractException;

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException;
}
