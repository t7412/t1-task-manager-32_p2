package ru.t1.chubarov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error. ProjectId is empty.");
    }

}
