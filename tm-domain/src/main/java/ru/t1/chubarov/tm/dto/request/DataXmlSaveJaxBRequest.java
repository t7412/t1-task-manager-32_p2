package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chubarov.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class DataXmlSaveJaxBRequest extends AbstractUserRequest {
}
