package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        @NotNull final String userId = getUserId();
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER TASK INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber()-1;
        @NotNull final Task tasks = getTaskService().findOneByIndex(userId, index);
        showTask(userId,tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by index.";
    }

}
