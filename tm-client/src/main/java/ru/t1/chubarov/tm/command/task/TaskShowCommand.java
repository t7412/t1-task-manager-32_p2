package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.TaskListRequest;
import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskSort sort = TaskSort.toSort(sortType);
//        @NotNull final String userId = getUserId();
//        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());

        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setSort(sortType);
        @NotNull final List<Task> tasks = getTaskEndpoint().listTask(request).getTasks();

        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) return;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list task.";
    }

}
