package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.chubarov.tm.api.service.ITaskService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    public void renderTask(@NotNull final List<Task> tasks) {
        System.out.println("[TASK LIST]");
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) return;
            System.out.println(index + ". " + task.getName());
            System.out.println("    id: " + task.getId());
            System.out.println("    status: " + task.getStatus());
            System.out.println("    desc: " + task.getDescription());
            index++;
        }
    }

    public void showTask(@Nullable final String userId, @Nullable final Task task) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }
    public void showTask(@Nullable final Task task) throws AbstractException {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
