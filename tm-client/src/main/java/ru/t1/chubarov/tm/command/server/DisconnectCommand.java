package ru.t1.chubarov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }

    @Nullable
    @Override
    public String getName() {
        return "disconnect";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Disconnect to server.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
