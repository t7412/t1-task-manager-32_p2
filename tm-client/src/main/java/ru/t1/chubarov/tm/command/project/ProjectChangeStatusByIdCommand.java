package ru.t1.chubarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();

        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setId(id);
        request.setStatus(status);
        getProjectEndpoint().changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by id.";
    }

}
