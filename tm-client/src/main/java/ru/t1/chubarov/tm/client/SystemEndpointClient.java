package ru.t1.chubarov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chubarov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chubarov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chubarov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.chubarov.tm.dto.response.ApplicationVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    public SystemEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
//        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
//        client.connect();
//        @NotNull final ApplicationAboutResponse serverAboutResponse = client.getAbout(new ApplicationAboutRequest());
//        System.out.println(serverAboutResponse.getEmail());
//        System.out.println(serverAboutResponse.getName());
//        @NotNull final ApplicationVersionResponse serverVersionResponse = client.getVersion(new ApplicationVersionRequest());
//        System.out.println(serverVersionResponse.getVersion());
//        client.disconnect();
    }

}
