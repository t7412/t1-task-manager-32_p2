package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.UserLoginRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "login";
    @NotNull
    private final String DESCRIPTION = "User login.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        request.setLogin(login);
        request.setPassword(password);
        getAuthEndPoint().login(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
