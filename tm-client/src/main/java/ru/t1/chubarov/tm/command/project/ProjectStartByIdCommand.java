package ru.t1.chubarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
//        @NotNull final String userId = getUserId();
//        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project status to [In progress] by id.";
    }

}
