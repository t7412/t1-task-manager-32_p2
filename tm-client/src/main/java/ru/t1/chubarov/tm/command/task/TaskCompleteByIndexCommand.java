package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY INDEX");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task status to [Completed] by index.";
    }

}
