package ru.t1.chubarov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.UserLoginResponse;
import ru.t1.chubarov.tm.dto.response.UserLogoutResponse;
import ru.t1.chubarov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    @Override
    @NotNull
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @NotNull
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
//        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
//        authEndpointClient.connect();
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
//
//        System.out.println(authEndpointClient.login(new UserLoginRequest("test2", "test2")));
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
//        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")));
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
//
//        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
//        authEndpointClient.disconnect();
    }

}
