package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "view-user-profile";
    @NotNull
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() throws AbstractException {
//        @NotNull final User user = serviceLocator.getAuthService().getUser();
//        System.out.println("[VIEW USER PROFILE]");
//        System.out.println("ID: " + user.getId());
//        System.out.println("LOGIN: " + user.getLogin());
//        System.out.println("FIRST NAME: " + user.getFirstName());
//        System.out.println("LAST NAME: " + user.getLastName());
//        System.out.println("MIDDLE NAME: " + user.getMiddleName());
//        System.out.println("EMAIL: " + user.getEmail());
//        System.out.println("ROLE: " + user.getRole());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
