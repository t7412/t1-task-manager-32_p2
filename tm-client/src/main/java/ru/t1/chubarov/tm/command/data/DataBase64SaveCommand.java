package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.DataBase64SaveRequest;
import ru.t1.chubarov.tm.enumerated.Role;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-base64";
    @NotNull
    private final String DESCRIPTION = "Save data to base64 file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDomainEndpoint().saveDataBase64(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
