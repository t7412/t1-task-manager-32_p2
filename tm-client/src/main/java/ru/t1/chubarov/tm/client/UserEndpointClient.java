package ru.t1.chubarov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.api.endpoint.IUserEndpoint;
import ru.t1.chubarov.tm.api.endpoint.IUserEndpointClient;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    public UserEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserUpdateProfileResponse updateProfileUser(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserProfileResponse viewProfileUser(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

}
