package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.DataXmlLoadFasterXmlRequest;
import ru.t1.chubarov.tm.enumerated.Role;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-xml-faster";
    @NotNull
    private final String DESCRIPTION = "Load data from xml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest();
        getDomainEndpoint().loadDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
