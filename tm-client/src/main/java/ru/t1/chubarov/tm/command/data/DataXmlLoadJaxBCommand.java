package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.chubarov.tm.enumerated.Role;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-xml-jaxb";
    @NotNull
    private final String DESCRIPTION = "Load data from xml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest();
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
