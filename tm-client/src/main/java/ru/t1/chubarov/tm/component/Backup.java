package ru.t1.chubarov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.command.data.AbstractDataCommand;
import ru.t1.chubarov.tm.command.data.DataBackupLoadCommand;
import ru.t1.chubarov.tm.command.data.DataBackupSaveCommand;
import ru.t1.chubarov.tm.exception.AbstractException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start() {
        load();
        es.scheduleWithFixedDelay(this::start,
                0,
                3,
                TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() throws AbstractException, IOException {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() throws AbstractException, IOException {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

}
