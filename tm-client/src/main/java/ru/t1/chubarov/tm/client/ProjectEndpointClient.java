package ru.t1.chubarov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectShowResponse listProject(@NotNull final ProjectShowRequest request) {
        return call(request, ProjectShowResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectTaskBindToProjectResponse bindTaskToProject(@NotNull final ProjectTaskBindToProjectRequest request) {
        return call(request, ProjectTaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public ProjectTaskUnbindToProjectResponse unbindTaskToProject(@NotNull final ProjectTaskUnbindToProjectRequest request) {
        return call(request, ProjectTaskUnbindToProjectResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] argd) {
//        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
//        authEndpointClient.connect();
//        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")));
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
//
//        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClient(authEndpointClient);
//        System.out.println(projectClient.createProject(new ProjectCreateRequest("HELLO", "WORLD")));
//        System.out.println(projectClient.listProject(new ProjectShowRequest()).getProjects());
//
//        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
//        authEndpointClient.disconnect();
    }

}
