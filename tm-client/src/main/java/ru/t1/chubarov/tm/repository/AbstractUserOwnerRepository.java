package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Nullable
    @Override
    public M add(@Nullable final String userId, M model) {
        if (userId == null) return null;
        model.setUserid(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty() || id == null) return null;
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        @Nullable final List<M> model = findAll(userId);
        removeAll();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .count();
    }

}
