package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY INDEX");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);

        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

}
