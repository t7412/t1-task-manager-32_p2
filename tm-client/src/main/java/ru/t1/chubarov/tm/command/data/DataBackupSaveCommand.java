package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.DataBackupSaveRequest;
import ru.t1.chubarov.tm.enumerated.Role;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";
    @NotNull
    private final String DESCRIPTION = "Save data backup to file.";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpoint().saveDataBackup(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
