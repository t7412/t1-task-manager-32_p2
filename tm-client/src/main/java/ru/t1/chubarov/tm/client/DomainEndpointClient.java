package ru.t1.chubarov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpoint {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
//        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
//        authEndpointClient.connect();
//        {
//            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")));
//            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
//            domainClient.saveDataBase64(new DataBase64SaveRequest());
//        }
//        {
//            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")));
//            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
//            domainClient.saveDataBase64(new DataBase64SaveRequest());
//        }
//        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
//        authEndpointClient.disconnect();
    }

}
