package ru.t1.chubarov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        @NotNull final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        System.out.println("socket: "+socket);
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
    }

    @Nullable
    @Override
    public String getName() {
        return "connect";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Connect to server.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
